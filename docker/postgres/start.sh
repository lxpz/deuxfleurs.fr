#!/bin/bash

if [ -f /local/pg_hba.conf ]; then
  echo "Copying Nomad configuration..."
  cp /local/pg_hba.conf /etc/postgresql/9.6/main/
  echo "Done"
fi


if [ -z "$(ls -A /var/lib/postgresql/9.6/main)" ]; then
  echo "Copying base"
  cp -r /var/lib/postgresql/9.6/base/* /var/lib/postgresql/9.6/main
  echo "Done"
fi

chmod -R 700  /var/lib/postgresql/9.6/main
chown -R postgres /var/lib/postgresql/9.6/main

echo "Starting postgres..."
. /usr/share/postgresql-common/init.d-functions
start 9.6
tail -f /var/log/postgresql/postgresql-9.6-main.log
