#!/bin/bash

if [[ ! -f /etc/ssl/certs/dovecot.crt || ! -f /etc/ssl/private/dovecot.key ]]; then
  cd /root
  openssl req \
    -new \
    -newkey rsa:4096 \
    -days 3650 \
    -nodes \
    -x509 \
    -subj ${TLSINFO} \
    -keyout dovecot.key \
    -out dovecot.crt
  
  mkdir -p /etc/ssl/{certs,private}/

  cp dovecot.crt /etc/ssl/certs/dovecot.crt 
  cp dovecot.key /etc/ssl/private/dovecot.key 
  chmod 400 /etc/ssl/certs/dovecot.crt 
  chmod 400 /etc/ssl/private/dovecot.key
fi

if [[ $(stat -c '%U' /var/mail/) != "mailstore" ]]; then
  chown -R mailstore /var/mail
fi

exec "$@"
