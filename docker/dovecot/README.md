```
sudo docker build -t superboum/amd64_dovecot:v2 .
```


```
sudo docker run -t -i \
  -e TLSINFO="/C=FR/ST=Bretagne/L=Rennes/O=Deuxfleurs/CN=www.deuxfleurs.fr" \
  -p 993:993 \
  -p 143:143 \
  -p 24:24 \
  -p 1337:1337 \
  -v /mnt/glusterfs/email/ssl:/etc/ssl/ \
  -v /mnt/glusterfs/email/mail:/var/mail \
  -v `pwd`/dovecot-ldap.conf:/etc/dovecot/dovecot-ldap.conf \
  superboum/amd64_dovecot:v1 \
  dovecot -F
```
