```
sudo docker build -t superboum/amd64_mariadb:v3 .

sudo docker run \
  -t -i \
  -p 3306:3306 \
  -v /tmp/mysql:/var/lib/mysql \
  -e LDAP_URI='ldap://bottin.service.2.cluster.deuxfleurs.fr' \
  -e LDAP_BASE='ou=users,dc=deuxfleurs,dc=fr' \
  -e LDAP_VERSION=3 \
  -e LDAP_BIND_DN='cn=admin,dc=deuxfleurs,dc=fr' \
  -e LDAP_BIND_PW='xxxx' \
  -e MYSQL_PASSWORD='xxxx' \
  superboum/amd64_mariadb:v1 \
  tail -f /var/log/mysql/error.log

CREATE USER quentin@localhost IDENTIFIED VIA pam USING 'mariadb';

```
