'use strict'

import io from './io.mjs'
import iptables from './iptables.mjs'

(async () => {
  const res = await [
    ...io,
    ...iptables
  ].map(async f => {
    try { 
      await f()
      return 'passed'
    } 
    catch(e) { 
      console.error(e)
      return 'failed'
    }
  }).reduce(async (acc, r) => {
    const accumulator = await acc
    const result = await r
    accumulator.total++
    accumulator[result]++
    return accumulator
  }, {total: 0, passed: 0, failed: 0})

  console.log(`Done. passed: ${res.passed}, failed: ${res.failed}, total: ${res.total}`)
})()
