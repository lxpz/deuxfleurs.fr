import chai from 'chai'
import { readFile } from '../src/io/files.mjs'
const expect = chai.expect

export default [
  (async () => {
    const dirname = import.meta.url.replace(/^file:\/\//g, '').replace(/io.mjs$/g, '')
    expect(await readFile(`${dirname}/../package.json`, 'utf-8')).to.include('Quentin')
  })
]
