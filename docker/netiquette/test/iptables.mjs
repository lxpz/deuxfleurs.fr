'use strict'

import chai from 'chai'
import iptables from '../src/injector/iptables.mjs'
const expect = chai.expect

export default [
  (async () => {
    const effective_actions = []
    const expected_actions = [
      'iptables -A INPUT -p tcp --dport 56 -j ACCEPT',
      'iptables -A INPUT -p tcp --dport 53 -j ACCEPT',
      'iptables -A INPUT -p udp --match multiport --dports 25630:25999 -j ACCEPT',
      'iptables -D INPUT -p tcp --dport 54 -j ACCEPT'
    ] 

    const mockLog = () => {}
    const mockReadFile = (file, opt) => '-A INPUT -p tcp --dport 53 -j ACCEPT'
    const mockExecCommand = (cmd, opts) => {
      if (cmd.match(/^iptables -S/g)) return { stdout: '-A INPUT -p tcp --dport 54 -j ACCEPT' }
      else effective_actions.push(cmd)
      return { stdout: '' } }

    const fw = await iptables('static', mockReadFile, mockExecCommand, mockLog)
    await fw(['public_port=56/tcp', 'public_port=25630-25999/udp', 'public_port=13', 'traefik.entrypoints=Host:im.deuxfleurs.fr;PathPrefix:/_matrix'])
    expect(effective_actions).to.have.members(expected_actions)  
  })
]
