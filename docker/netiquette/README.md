# netiquette

```
npm install
npm test
```

You will probably need to run consul in parallel:

```
consul agent -dev
```

You can register services like that:

```
consul services register -name=toto -tag="public_port=4848"
```

You will need some arguments to run the software:

```
sudo npm start node=rincevent ipt_base=./static.iptables
```
