'use strict'

let l
export default l = async (node, consul, log, notify) => {
  const watch = consul.watch({ method: consul.catalog.node.services, options: {node: node}})

  const extract_tags = data => 
    data ? 
      Object
        .keys(data.Services)
        .map(k => data.Services[k].Tags)
        .reduce((acc, v) => [...acc, ...v], []) :
      []

  watch.on('error', err => {
    console.error('error', err)
  })

  watch.on('change', async (data, res) => {
    try {
      const tags = extract_tags(data)
      log(`[consul] new update, detected ${tags.length} tags`)
      await notify(tags)
    } catch(e) {
      console.error('failed to notify target', e)
    }
  })
  
  log('[consul] initialized')
}
