'use strict'

let l
export default l = async (timer, interval, notify) => {
  timer(() => {
    notify([])
    console.log(`[control_loop] actuation (triggered every ${interval} ms)`)
  }, interval)
  console.log("[control_loop] initialized")
}
