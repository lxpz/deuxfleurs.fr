'use strict'

import child_process from 'child_process'

export const exec = (cmd, opts) =>
  new Promise((resolve, reject) =>
    child_process.exec(cmd, opts, (error, stdout, stderr) =>
      error ? reject({err: error, stdout: stdout, stderr: stderr}) : resolve({stdout: stdout, stderr: stderr})))

