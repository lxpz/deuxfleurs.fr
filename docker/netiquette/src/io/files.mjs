'use strict'

import fs from 'fs'

export const readFile = (file, opts) => 
  new Promise((resolve, reject) =>
    fs.readFile(file, opts, (err, data) => 
      err ? reject(err) : resolve(data)))
