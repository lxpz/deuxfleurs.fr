
```bash
sudo docker build -t superboum/amd64_seafile:v5 .
```

When upgrading, connect on a production server and run:

```bash
nomad stop seafile
sudo docker build -t superboum/amd64_seafile:v6 .

sudo docker run -t -i \
  -v /mnt/glusterfs/seafile:/mnt/seafile-data \
  -v /mnt/glusterfs/seaconf/conf:/srv/webstore/conf \
  -v /mnt/glusterfs/seaconf/ccnet:/srv/webstore/ccnet \
  superboum/amd64_seafile:v5

# See:
#  * https://download.seafile.com/published/seafile-manual/deploy/upgrade.md
#  * https://download.seafile.com/published/seafile-manual/changelog/server-changelog.md



nomad start seafile.hcl
```

when upgrading, change the command on start
