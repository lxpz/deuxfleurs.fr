deuxfleurs.fr
=============

*Many things are still missing here, including a proper documentation. Please stay nice, it is a volunter project. Feel free to open pull/merge requests to improve it. Thanks.*

## Our abstraction stack

We try to build a generic abstraction stack between our different resources (CPU, RAM, disk, etc.) and our services (Chat, Storage, etc.):

  * ansible (physical node conf)
  * nomad (schedule containers)
  * consul (distributed key value store / lock / service discovery)
  * glusterfs (file storage)
  * stolon + postgresql (distributed relational database)
  * docker (container tool)
  * bottin (LDAP server, auth)

Some services we provide:

  * Chat (Matrix/Riot)
  * Email (Postfix/Dovecot/Sogo)
  * Storage (Seafile)

As a generic abstraction is provided, deploying new services should be easy.

## Start hacking

### Clone the repository

```
git clone https://gitlab.com/superboum/deuxfleurs.fr.git
git submodule init
git submodule update
```

### Deploying/Updating new services is done from your machine

*The following instructions are provided for ops that already have access to the servers.*

Deploy Nomad on your machine:

```bash
export NOMAD_VER=0.9.1
wget https://releases.hashicorp.com/nomad/${NOMAD_VER}/nomad_${NOMAD_VER}_linux_amd64.zip
unzip nomad_${NOMAD_VER}_linux_amd64.zip
sudo mv nomad /usr/local/bin
rm nomad_${NOMAD_VER}_linux_amd64.zip
```

Deploy Consul on your machine:

```bash
export CONSUL_VER=1.5.1
wget https://releases.hashicorp.com/consul/${CONSUL_VER}/consul_${CONSUL_VER}_linux_amd64.zip
unzip consul_${CONSUL_VER}_linux_amd64.zip
sudo mv consul /usr/local/bin
rm consul_${CONSUL_VER}_linux_amd64.zip
```

Create an alias (and put it in your `.bashrc`) to bind APIs on your machine:

```
alias bind_df="ssh \
  -p110 \
  -N \
  -L 4646:127.0.0.1:4646 \
  -L 8500:127.0.0.1:8500 \
  -L 8082:traefik.service.2.cluster.deuxfleurs.fr:8082 \
  <a server from the cluster>"
```

and run: 

```
bind_df
```
