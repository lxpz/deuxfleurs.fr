#===
# Base configuration
#===
myhostname = smtp.deuxfleurs.fr
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
mydestination = smtp.deuxfleurs.fr
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 192.168.1.0/24
mailbox_size_limit = 0
recipient_delimiter = +
inet_protocols = all
inet_interfaces = all
message_size_limit = 204800000
smtpd_banner = $myhostname
biff = no
append_dot_mydomain = no
readme_directory = no
compatibility_level = 2

#===
# TLS parameters
#===
smtpd_tls_cert_file=/etc/ssl/certs/postfix.crt
smtpd_tls_key_file=/etc/ssl/private/postfix.key
smtpd_use_tls=yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
#smtp_tls_policy_maps = hash:/etc/postfix/tls_policy
smtp_tls_security_level = may

#===
# Remove privacy related content from emails
#===
mime_header_checks = regexp:/etc/postfix/header_checks
header_checks = regexp:/etc/postfix/header_checks

#===
# Handle user authentication (handled by dovecot)
#===
smtpd_sasl_auth_enable = yes
smtpd_sasl_path = inet:dovecot-auth.service.2.cluster.deuxfleurs.fr:1337
smtpd_sasl_type = dovecot

#===
# Restrictions / Checks
#===
# -- Inspired by: http://www.postfix.org/SMTPD_ACCESS_README.html#lists

# Require a valid HELO
smtpd_helo_required = yes
# As we use the same postfix to send and receive,
# we can't enforce a valid HELO hostname...
#smtpd_helo_restrictions = 
#  reject_unknown_helo_hostname

# Require that sender email has a valid domain
smtpd_sender_restrictions = 
  reject_unknown_sender_domain 

# Delivering email policy
# MyNetwork is required by sogo
smtpd_recipient_restrictions =
  permit_sasl_authenticated
  permit_mynetworks
  reject_unauth_destination
  reject_rbl_client zen.spamhaus.org
  reject_rhsbl_reverse_client dbl.spamhaus.org
  reject_rhsbl_helo dbl.spamhaus.org
  reject_rhsbl_sender dbl.spamhaus.org

# Sending email policy
# MyNetwork is required by sogo
smtpd_relay_restrictions =
  permit_sasl_authenticated
  permit_mynetworks
  reject_unauth_destination

smtpd_data_restrictions = reject_unauth_pipelining

smtpd_client_connection_rate_limit = 2

#===
# Rate limiting
#===
slow_destination_recipient_limit = 20
slow_destination_concurrency_limit = 2

#====
# Transport configuration
#====
transport_maps = hash:/etc/postfix/transport
virtual_mailbox_domains = deuxfleurs.fr, dufour.io, dufour.tk
virtual_mailbox_maps = ldap:/etc/postfix/ldap-account.cf
#virtual_alias_domains = deuxfleurs.fr, dufour.io, dufour.tk
virtual_alias_maps = ldap:/etc/postfix/ldap-alias.cf
virtual_transport = lmtp:dovecot-lmtp.service.2.cluster.deuxfleurs.fr:24
#master_service_disable = 
#tcp_windowsize = 1400

#===
# Mail filters
#===
milter_default_action = accept
milter_protocol = 6
smtpd_milters = inet:opendkim.service.2.cluster.deuxfleurs.fr:8999
non_smtpd_milters = inet:opendkim.service.2.cluster.deuxfleurs.fr:8999
