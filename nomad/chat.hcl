job "chat" {
  datacenters = ["dc1"]
  type = "service"

  group "matrix" {
    count = 1
    task "synapse" {
      driver = "docker"

      config {
        image = "superboum/amd64_synapse:v24"
        readonly_rootfs = true
        port_map {
          client_port = 8008
        }
        command = "python"
        args = [
          "-m", "synapse.app.homeserver",
          "-n",
          "-c", "/etc/matrix-synapse/homeserver.yaml"
        ]
        volumes = [
          "secrets/conf:/etc/matrix-synapse",
          "/mnt/glusterfs/chat/matrix/synapse/media:/var/lib/matrix-synapse/media",
          "/mnt/glusterfs/chat/matrix/synapse/uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse-logs:/var/log/matrix-synapse",
          "/tmp/synapse:/tmp"
        ]
      }
      
      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/chat/synapse/homeserver.yaml?raw"
        destination = "secrets/tpl/homeserver.yaml.tpl"
        mode = "file"
      }
      template {
        source = "secrets/tpl/homeserver.yaml.tpl"
        destination = "secrets/conf/homeserver.yaml"
      }

      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/chat/fb2mx/registration.yaml?raw"
        destination = "secrets/tpl/fb2mx_registration.yaml.tpl"
        mode = "file"
      }
      template {
        source = "secrets/tpl/fb2mx_registration.yaml.tpl"
        destination = "secrets/conf/fb2mx_registration.yaml"
      }

      template {
        data = "{{ key \"configuration/chat/synapse/log.yaml\" }}"
        destination = "secrets/conf/log.yaml"
      }
      template {
        data = "{{ key \"configuration/chat/synapse/conf.d/server_name.yaml\" }}"
        destination = "secrets/conf/server_name.yaml"
      }
      template {
        data = "{{ key \"configuration/chat/synapse/conf.d/report_stats.yaml\" }}"
        destination = "secrets/conf/report_stats.yaml"
      }
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.crt\" }}"
        destination = "secrets/conf/homeserver.tls.crt"
      }
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.dh\" }}"
        destination = "secrets/conf/homeserver.tls.dh"
      }
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.tls.key\" }}"
        destination = "secrets/conf/homeserver.tls.key"
      }
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.signing.key\" }}"
        destination = "secrets/conf/homeserver.signing.key"
      }

      env {
        SYNAPSE_CACHE_FACTOR = 1
      }

      resources {
        cpu = 1000
        memory = 4000
        network {
          port "client_port" { }
        }
      }

      service {
        name = "synapse"
        port = "client_port"
        address_mode = "host"
        tags = [
          "matrix",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:im.deuxfleurs.fr,deuxfleurs.fr;PathPrefix:/_matrix"
        ]
        check {
          type = "tcp"
          port = "client_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
/*
  group "facebook_bridge" {
    count = 1
    task "fb2mx" {
      driver = "docker"
      config {
        # https://mau.dev/tulir/mautrix-facebook/container_registry
        image = "dock.mau.dev/tulir/mautrix-facebook:latest"
        port_map {
          api_port = 29319
        }
        volumes = [
	  "secrets/conf:/data"
        ]
      }

      resources {
        memory = 50
        network {
          port "api_port" {
            static = "29319"
          }
        }
      }

      service {
        tags = ["mxbridge"]
        port = "api_port"
        address_mode = "host"
        name = "fb2mx"
        check {
          type = "tcp"
          port = "api_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/chat/fb2mx/registration.yaml?raw"
        destination = "secrets/tpl/registration.yaml.tpl"
        mode = "file"
      }
      template {
        source = "secrets/tpl/registration.yaml.tpl"
        destination = "secrets/conf/registration.yaml"
      }

      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/chat/fb2mx/config.yaml?raw"
        destination = "secrets/tpl/config.yaml.tpl"
        mode = "file"
      }
      template {
        source = "secrets/tpl/config.yaml.tpl"
        destination = "secrets/conf/config.yaml"
      }
    }
 }
*/
  group "riotweb" {
    count = 1

    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_riotweb:v8"
        port_map {
          web_port = 8043
        }
        volumes = [
	  "secrets/config.json:/srv/http/config.json"
        ]
      }

      template {
        data = "{{ key \"configuration/chat/riot_web/config.json\" }}"
        destination   = "secrets/config.json"
      }

      resources {
        memory = 21
        network {
          port "web_port" {}
        }
      }

      service {
        tags = [
          "webstatic",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:im.deuxfleurs.fr,riot.deuxfleurs.fr;PathPrefix:/"
        ]
        port = "web_port"
        address_mode = "host"
        name = "webstatic"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }

/*
  group "turn_stun" {
    count=1
    task "coturn" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/superboum/ankh-morpork/amd64_coturn:v1"
        port_map {
          main_port = 3478
          alt_port = 3479
        }
        command = "/usr/bin/turnserver"
        args = [
          "-X", "82.253.205.190",
          "-v",
          "-f",
          "-a"
        ]
        volumes = [
          "secrets/turnserver.conf:/etc/turnserver.conf"
        ] 
      }
      
      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/chat/coturn/turnserver.conf.tpl?raw"
        destination = "secrets/turnserver.conf.tpl"
        mode = "file"
      }

      template {
        source = "secrets/turnserver.conf.tpl"
        destination = "secrets/turnserver.conf"
      }

      resources {
        memory = 50
        network {
          port "main_port" { 
            static = "3478"
          }
          port "alt_port" {
            static = "3479"
          }
        }
      }

       service {
        tags = [
          "coturn",
          "matrix"
        ]
        port = "main_port"
        address_mode = "host"
        name = "coturn"
        check {
          type = "tcp"
          port = "main_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

    }
  }
*/

}

