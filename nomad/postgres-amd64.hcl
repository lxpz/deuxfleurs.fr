job "dbx86" {
  datacenters = ["dc1"]
  type = "system"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  update {
    max_parallel     = 1
    stagger = "5m"
  }

  group "postgres" {
    task "sentinel" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v3"
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-sentinel"
        args = [
          "--cluster-name", "pissenlit",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
        ]
      }
      resources {
        memory = 100
      }
    }

    task "proxy" {
      driver = "docker"

      config {
        image = "superboum/amd64_postgres:v3"
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-proxy"
        args = [
          "--cluster-name", "pissenlit",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
          "--port", "5432",
          "--listen-address", "0.0.0.0"
        ]
        port_map {
          psql_proxy_port = 5432
        }
      }

      resources {
        memory = 100
        network {
          port "psql_proxy_port" {
            static = 5432
          }
        }
      }

      service {
        tags = ["sql"]
        port = "psql_proxy_port"
        address_mode = "host"
        name = "psql-proxy"
        check {
          type = "tcp"
          port = "psql_proxy_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "10m"
            ignore_warnings = false
          }
        }
      }
    }

    task "keeper" {
      driver = "docker"

      config {
        network_mode = "host" 
        image = "superboum/amd64_postgres:v3"
        readonly_rootfs = false
        command = "/usr/local/bin/stolon-keeper"
        args = [
          "--cluster-name", "pissenlit",
          "--store-backend", "consul",
          "--store-endpoints", "http://consul.service.2.cluster.deuxfleurs.fr:8500",
          "--data-dir", "/mnt/persist",
          "--pg-su-password", "${PG_SU_PWD}",
          "--pg-repl-username", "${PG_REPL_USER}",
          "--pg-repl-password", "${PG_REPL_PWD}",
          "--pg-listen-address", "${attr.unique.network.ip-address}",
          "--pg-port", "5433",
          "--pg-bin-path", "/usr/lib/postgresql/9.6/bin/"
        ]
        port_map {
          psql_port = 5433
        }
        volumes = [
          "/mnt/ssd/postgres:/mnt/persist"
        ]
      }
      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/postgres/keeper/env.tpl?raw"
        destination = "secrets/env.tpl"
        mode = "file"
      }
      template {
        source = "secrets/env.tpl"
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 500
        network {
          port "psql_port" {
            static = "5433"
          }
        }
      }

      service {
        tags = ["sql"]
        port = "psql_port"
        address_mode = "host"
        name = "keeper"
        check {
          type = "tcp"
          port = "psql_port"
          interval = "60s"
          timeout = "5s"

          check_restart {
            limit = 3
            grace = "60m"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

