job "email" {
  datacenters = ["dc1"]
  type = "service"

  group "dovecot" {
    count = 1
    task "server" {
      driver = "docker"

      config {
        image = "superboum/amd64_dovecot:v2"
        readonly_rootfs = false
        port_map {
          auth_port = 1337
          imaps_port = 993
          imap_port = 143
          lmtp_port = 24
        }
        command = "dovecot"
        args = [ "-F" ]
        volumes = [
          "secrets/ssl/certs:/etc/ssl/certs",
          "secrets/ssl/private:/etc/ssl/private",
	  "secrets/conf/dovecot-ldap.conf:/etc/dovecot/dovecot-ldap.conf",
          "/mnt/glusterfs/email/mail:/var/mail/",
        ]
      }

      env {
        TLSINFO = "/C=FR/ST=Bretagne/L=Rennes/O=Deuxfleurs/CN=imap.deuxfleurs.fr"
      }

      resources {
        cpu = 100
        memory = 200
        network {
          port "auth_port" {
            static = "1337"
          }
          port "imap_port" {
            static = "143"
          }
          port "imaps_port" {
            static = "993"
          }
          port "lmtp_port" {
            static = "24"
          }
        }
      }

      service {
        name = "dovecot-auth"
        port = "auth_port"
        address_mode = "host"
        tags = [
          "dovecot",
        ]
        check {
          type = "tcp"
          port = "auth_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "dovecot-imap"
        port = "imap_port"
        address_mode = "host"
        tags = [
          "dovecot",
        ]
        check {
          type = "tcp"
          port = "imap_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "dovecot-imaps"
        port = "imaps_port"
        address_mode = "host"
        tags = [
          "dovecot",
        ]

        check {
          type = "tcp"
          port = "imaps_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "dovecot-lmtp"
        port = "lmtp_port"
        address_mode = "host"
        tags = [
          "dovecot",
        ]

        check {
          type = "tcp"
          port = "lmtp_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      template {
        data = "{{ key \"configuration/email/dovecot/dovecot.crt\" }}"
        destination = "secrets/ssl/certs/dovecot.crt"
        perms = "400"
      }
      template {
        data = "{{ key \"configuration/email/dovecot/dovecot.key\" }}"
        destination = "secrets/ssl/private/dovecot.key"
        perms = "400"
      }
      template {
        data = "{{ key \"configuration/email/dovecot/dovecot-ldap.conf\" }}"
        destination = "secrets/conf/dovecot-ldap.conf"
        perms = "400"
      }
    }
  }

  group "opendkim" {
    count = 1
    task "server" {
      driver = "docker"

      config {
        image = "superboum/amd64_opendkim:v1"
        readonly_rootfs = false
        port_map {
          dkim_port = 8999
        }
        command = "opendkim"
        args = [ "-f", "-v", "-x", "/etc/opendkim.conf" ]
        volumes = [
          "secrets/dkim:/etc/dkim",
          "/dev/log:/dev/log",
        ]
      }

      resources {
        cpu = 100
        memory = 50
        network {
          port "dkim_port" {
            static = "8999"
          }
        }
      }

      service {
        name = "opendkim"
        port = "dkim_port"
        address_mode = "host"
        tags = [
          "opendkim",
        ]
        check {
          type = "tcp"
          port = "dkim_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      template {
        data = "{{ key \"configuration/email/dkim/keytable\" }}"
        destination = "secrets/dkim/keytable"
      }
      template {
        data = "{{ key \"configuration/email/dkim/signingtable\" }}"
        destination = "secrets/dkim/signingtable"
      }
      template {
        data = "{{ key \"configuration/email/dkim/smtp.private\" }}"
        destination = "secrets/dkim/smtp.private"
        perms = "600"
      }
      template {
        data = "{{ key \"configuration/email/dkim/smtp.txt\" }}"
        destination = "secrets/dkim/smtp.txt"
      }
      template {
        data = "{{ key \"configuration/email/dkim/trusted\" }}"
        destination = "secrets/dkim/trusted"
      }
    }
  }

  group "postfix" {
    count = 1
    task "server" {
      driver = "docker"

      config {
        image = "superboum/amd64_postfix:v1"
        readonly_rootfs = false
        port_map {
          smtp_port = 25
          smtps_port = 465
          submission_port = 587
        }
        command = "postfix"
        args = [ "start-fg" ]
        volumes = [
          "secrets/ssl/certs:/etc/ssl/certs",
          "secrets/ssl/private:/etc/ssl/private",
	  "secrets/postfix:/etc/postfix-conf",
          "/dev/log:/dev/log"
        ]
      }

      env {
        TLSINFO = "/C=FR/ST=Bretagne/L=Rennes/O=Deuxfleurs/CN=smtp.deuxfleurs.fr"
        MAILNAME = "smtp.deuxfleurs.fr",
      }

      resources {
        cpu = 100
        memory = 50
        network {
          port "smtp_port" {
            static = "25"
          }
          port "smtps_port" {
            static = "465"
          }
          port "submission_port" {
            static = "587"
          }
        }
      }

      service {
        name = "postfix-smtp"
        port = "smtp_port"
        address_mode = "host"
        tags = [
          "postfix",
        ]
        check {
          type = "tcp"
          port = "smtp_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "postfix-smtps"
        port = "smtps_port"
        address_mode = "host"
        tags = [
          "postfix",
        ]

        check {
          type = "tcp"
          port = "smtps_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        name = "postfix-submission"
        port = "submission_port"
        address_mode = "host"
        tags = [
          "postfix",
        ]

        check {
          type = "tcp"
          port = "submission_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      template {
        data = "{{ key \"configuration/email/postfix/postfix.crt\" }}"
        destination = "secrets/ssl/certs/postfix.crt"
        perms = "400"
      }
      template {
        data = "{{ key \"configuration/email/postfix/postfix.key\" }}"
        destination = "secrets/ssl/private/postfix.key"
        perms = "400"
      }
      template {
        data = "{{ key \"configuration/email/postfix/dynamicmaps.cf\" }}"
        destination = "secrets/postfix/dynamicmaps.cf"
      }
      template {
        data = "{{ key \"configuration/email/postfix/header_checks\" }}"
        destination = "secrets/postfix/header_checks"
      }
      template {
        data = "{{ key \"configuration/email/postfix/ldap-account.cf\" }}"
        destination = "secrets/postfix/ldap-account.cf"
      }
      template {
        data = "{{ key \"configuration/email/postfix/ldap-alias.cf\" }}"
        destination = "secrets/postfix/ldap-alias.cf"
      }
      template {
        data = "{{ key \"configuration/email/postfix/main.cf\" }}"
        destination = "secrets/postfix/main.cf"
      }
      template {
        data = "{{ key \"configuration/email/postfix/master.cf\" }}"
        destination = "secrets/postfix/master.cf"
      }
      template {
        data = "{{ key \"configuration/email/postfix/transport\" }}"
        destination = "secrets/postfix/transport"
      }
      template {
        data = "{{ key \"configuration/email/postfix/transport.db\" }}"
        destination = "secrets/postfix/transport.db"
      }
    }
  }

  group "sogo" {
    count = 1
    task "bundle" {
    
      driver = "docker"

      config {
        image = "superboum/amd64_sogo:v6"
        readonly_rootfs = false
        port_map {
          sogo_web_port = 8080
        }
        volumes = [
	  "secrets/sogo.conf:/etc/sogo/sogo.conf",
        ]
      }

      /* Workaround as there is no consul source and no way to template recursively... */
      artifact {
        source = "http://127.0.0.1:8500/v1/kv/configuration/email/sogo/sogo.conf.tpl?raw"
        destination = "secrets/tpl/sogo.conf.tpl"
        mode = "file"
      }
      template {
        source = "secrets/tpl/sogo.conf.tpl"
        destination = "secrets/sogo.conf"
      }

      resources {
        cpu = 200
        memory = 1000
        network {
          port "sogo_web_port" {}
        }
      }
      
      service {
        name = "sogo"
        port = "sogo_web_port"
        address_mode = "host"
        tags = [
          "sogo",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:sogo.deuxfleurs.fr;PathPrefix:/"
        ]
        check {
          type = "tcp"
          port = "sogo_web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "5m"
            ignore_warnings = false
          }
        }
      }
      
    }
  }
}
