job "web_static" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "landing" {
    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_landing:v8"
        port_map {
          web_port = 8043
        }
      }

      resources {
        memory = 20
        network {
          port "web_port" {}
        }
      }

      service {
        tags = [
          "webstatic",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:deuxfleurs.fr,www.deuxfleurs.fr,deuxfleurs.org,www.deuxfleurs.org;Path:/,/robots.txt,/landing/arobase.png"
        ]
        port = "web_port"
        address_mode = "host"
        name = "landing"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }

  /* To be replaced by a static site manager */
  group "quentin" {
    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_blog:v19"
        port_map {
          web_port = 8043
        }
      }

      resources {
        memory = 20
        network {
          port "web_port" {}
        }
      }

      service {
        tags = [
          "webstatic",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https",
          "traefik.frontend.rule=Host:quentin.dufour.io,www.quentin.dufour.io;PathPrefix:/"
        ]
        port = "web_port"
        address_mode = "host"
        name = "blog-quentin"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

