job "frontend" {
  datacenters = ["dc1"]
  type = "service"

  group "traefik" {
    task "server" {
      driver = "docker"

      config {
        image = "amd64/traefik:1.7.20"
        readonly_rootfs = true
        port_map {
          https_port = 443
          http_port = 80
          adm_port = 8082
          synapse_federation_frontend = 8448
        }
        volumes = [
          "secrets/traefik.toml:/etc/traefik/traefik.toml",
        ]
      }

      resources {
        memory = 101
        network {
          port "synapse_federation_frontend" {
            static = "8448"
          }
          port "https_port" {
            static = "443"
          }
          port "http_port" {
            static = "80"
          }
          port "adm_port" {
            static = "8082"
          }
        }
      }

      service {
        tags = ["https"]
        port = "https_port"
        address_mode = "host"
        name = "traefik"
        check {
          type = "tcp"
          port = "https_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
      
      template {
        data = "{{ key \"configuration/traefik/traefik.toml\" }}"
        destination = "secrets/traefik.toml"
      }
      template {
        data = "{{ key \"configuration/traefik/cloudflare.env\" }}"
        destination = "secrets/cloudflare.env"
        env = true
      }
    }
  }
}

