job "seafile" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    count = 1
    task "server" {
      driver = "docker"
      config {
        image = "superboum/amd64_seafile:v6"
        
        ## cmd + args are used for running an instance attachable for update
        # command = "/bin/sleep"
        # args = ["999999"]

        port_map {
          seahub_port = 8000
          seafdav_port = 8084
          seafhttp_port = 8082
        }

        volumes = [
          "/mnt/glusterfs/seafile:/mnt/seafile-data",
          "secrets/conf:/srv/webstore/conf",
          "secrets/ccnet:/srv/webstore/ccnet"
        ]
      }

      resources {
        memory = 2048
        network {
          port "seahub_port" {}
          port "seafhttp_port" {}
          port "seafdav_port" {}
        }
      }

      service {
        tags = [
          "seafile",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:cloud.deuxfleurs.fr;PathPrefix:/"
        ]
        port = "seahub_port"
        address_mode = "host"
        name = "seahub"
        check {
          type = "tcp"
          port = "seahub_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = [
          "seafile",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:cloud.deuxfleurs.fr;PathPrefixStrip:/seafhttp"

        ]
        port = "seafhttp_port"
        address_mode = "host"
        name = "seafhttp"
        check {
          type = "tcp"
          port = "seafhttp_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = [
          "seafile",
          "traefik.enable=true",
          "traefik.frontend.entryPoints=https,http",
          "traefik.frontend.rule=Host:cloud.deuxfleurs.fr;PathPrefix:/seafdav"

        ]
        port = "seafdav_port"
        address_mode = "host"
        name = "seafdav"
        check {
          type = "tcp"
          port = "seafdav_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      template {
        data = "{{ key \"configuration/seafile/ccnet/mykey.peer\" }}"
        destination = "secrets/ccnet/mykey.peer"
      }
      template {
        data = "{{ key \"configuration/seafile/ccnet/seafile.ini\" }}"
        destination = "secrets/ccnet/seafile.ini"
      }
      template {
        data = "{{ key \"configuration/seafile/conf/ccnet.conf\" }}"
        destination = "secrets/conf/ccnet.conf"
      }
      template {
        data = "{{ key \"configuration/seafile/conf/mykey.peer\" }}"
        destination = "secrets/conf/mykey.peer"
      }
      template {
        data = "{{ key \"configuration/seafile/conf/seafdav.conf\" }}"
        destination = "secrets/conf/seafdav.conf"
      }
      template {
        data = "{{ key \"configuration/seafile/conf/seafile.conf\" }}"
        destination = "secrets/conf/seafile.conf"
      }
      template {
        data = "{{ key \"configuration/seafile/conf/seahub_settings.py\" }}"
        destination = "secrets/conf/seahub_settings.py"
      }
      template {
        data = "{{ key \"configuration/seafile/conf/gunicorn.conf\" }}"
        destination = "secrets/conf/gunicorn.conf"
      }
    }
  }
}

