job "not_safe_object_storage" {
  datacenters = ["dc1"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "not_safe_pithos" {
    count = 2
    task "not_safe_server" {
      driver = "docker"
      config {
        image = "superboum/amd64_pithos:v1"
        readonly_rootfs = true
        port_map {
          s3_port = 8080
        }
        volumes = [
          "secrets/pithos.yaml:/etc/pithos/pithos.yaml"
        ] 
      }

      resources {
        memory = 500
        network {
          port "s3_port" {
            static = "8080"
          }
        }
      }

      template {
        data = <<EOH
service:
  host: '0.0.0.0'
  port: 8080


## logging configuration
## ---------------------
logging:
  level: info
  console: true
  files: []

# overrides:
#   io.exo.pithos: debug


## global options
## --------------
options:
  service-uri: 's3.esir.deuxfleurs.fr'
  reporting: true
  server-side-encryption: true
  multipart-upload: true
  masterkey-provisioning: true
  masterkey-access: true
  default-region: 'FR-RN1'


## keystore configuration
## ----------------------
#
# Keystores associate an access key with
# an organization and secret key.
#
# They may offer provisioning capacities with the
# masterkey. The default provider relies on keys
# being defined inline.
# generate access key: openssl rand -base64 24
# generate secret key: openssl rand -base64 39
# (size is arbitrary)
keystore:
  keys:
    NHu3glGc0lj5FL5AZPTvgjB20tb9w4Eo:
      master: true
      tenant: 'pyr@spootnik.org'
      secret: 'fpyehmZsimMHeYScjwTUREzvIOICeRZiO01Dck0JIKEifKdwOT3T'
    rXNoqKXY45RcxpBOKy8i4H8fqGzlHIZu:
      tenant: 'exoscale'
      secret: 'qtQlWujN70Ukh9IvIbqIM3Zqos/5aU72hOhLCXblQ0PmfYsGO8lU'


## bucketstore configuration
## -------------------------
#
# The bucketstore is ring global and contains information
# on bucket location and global parameters.
#
# Its primary aim is to hold bucket location and ownership
# information.
#
# The default provider relies on cassandra.
bucketstore:
  default-region: 'FR-RN1'
  cluster:
    - 148.60.11.181
    - 148.60.11.183
    - 148.60.11.237
  keyspace: 'storage'


## regions
## -------
#
# Regions are composed of a metastore and an arbitrary number
# of named storage classes which depend on a blobstore.
#
# The metastore holds metadata for the full region, as well as
# object storage-class placement information.
#
# The default implementation of both metastore and blobstore
# rely on cassandra.
#
regions:
  FR-RN1:
    metastore:
      cluster:
        - 148.60.11.181
        - 148.60.11.183
        - 148.60.11.237
      keyspace: 'storage'
    storage-classes:
      standard:
        cluster:
          - 148.60.11.181
          - 148.60.11.183
          - 148.60.11.237
        keyspace: 'storage'
        max-chunk: '128k'
        max-block-chunks: 1024
EOH
        destination = "secrets/pithos.yaml"
      }

      service {
        tags = ["pithos"]
        port = "s3_port"
        address_mode = "host"
        name = "pithos"
        check {
          type = "tcp"
          port = "s3_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "300s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

